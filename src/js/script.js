var intervalId = 0;

function scrollStep(scrollStepHeight) {
    if(window.pageYOffset === 0) {
        clearInterval(intervalId);
    }
    window.scrollTo(0, window.pageYOffset - scrollStepHeight);
}

function scrollToTop(scrollStepHeight, delay) {
    if(scrollStepHeight <= 0) {
        alert("The specified scroll step height must be positive!");
    } else if(delay <= 0) {
        alert("The specified scroll delay must be positive!");
    }
    intervalId = setInterval(function() {
        scrollStep(scrollStepHeight);
    }, delay);

}

document.addEventListener("DOMContentLoaded", function () {

    console.log('Ça marche!');

//--------------------------------------------------------------------------------------------------pop-up

    var boutonAjout = document.querySelector("#ajout-class");

    var boutonSupprime = document.querySelector("#supprime-class");


    boutonAjout.addEventListener("click",function (event) {

        event.preventDefault();

        document.querySelector('#modale').classList.toggle("u-hidden");
    });

    boutonSupprime.addEventListener("click",function (event) {

        event.preventDefault();

        document.querySelector('#modale').classList.toggle("u-hidden");
    });

//--------------------------------------------------------------------------------------------------scroll top


        retour.addEventListener("click", function() {
        scrollToTop(50, 16.6);});



});